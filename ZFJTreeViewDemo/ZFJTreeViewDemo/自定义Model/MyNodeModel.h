//
//  MyNodeModel.h
//  ZFJTreeViewDemo
//
//  Created by 张福杰 on 2019/6/27.
//  Copyright © 2019 张福杰. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 用户自己建的节点数据模型
 */
@interface MyNodeModel : NSObject

@property (nonatomic,  copy) NSString *title;//用户自定义字段

@end

NS_ASSUME_NONNULL_END
