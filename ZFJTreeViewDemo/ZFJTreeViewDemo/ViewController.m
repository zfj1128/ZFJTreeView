//
//  ViewController.m
//  ZFJTreeViewDemo
//
//  Created by 张福杰 on 2019/6/27.
//  Copyright © 2019 张福杰. All rights reserved.
//

#import "ViewController.h"
#import "ZFJTreeView.h"
#import "MyNodeModel.h"
#import "MyNodeViewCell.h"

@interface ViewController ()<ZFJTreeViewDelegate>

@property (nonatomic,strong) ZFJTreeView *treeView;
@property (nonatomic,strong) NSMutableArray *dataArr_1;//一级节点数组
@property (nonatomic,strong) NSMutableArray *dataArr_2;//二级节点数组
@property (nonatomic,strong) NSMutableArray *dataArr_3;//三级节点数组
@property (nonatomic,strong) NSMutableArray *dataArr_4;//四级节点数组
@property (nonatomic,strong) NSMutableArray *dataArr_5;//五级节点数组
@property (nonatomic,strong) NSMutableArray *dataArr_6;//六级节点数组
@property (nonatomic,strong) NSMutableArray *dataArr_7;//七级节点数组
@property (nonatomic,strong) NSMutableArray *dataArr_8;//八级节点数组
@property (nonatomic,strong) NSMutableArray *dataArr_9;//九级节点数组
@property (nonatomic,strong) NSMutableArray *dataArr_10;//十级节点数组

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self uiConfig];
}

- (void)uiConfig{
    self.treeView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:self.treeView];
    
    UIView *headerView = [[UIView alloc] init];
    headerView.frame = CGRectMake(0, 0, ScreenWidth, 100);
    headerView.backgroundColor = [UIColor yellowColor];
    self.treeView.headerView = headerView;
    
    MyNodeModel *myModel = [[MyNodeModel alloc] init];
    myModel.title = @"自定义Title";
    
    #pragma mark - 添加一级节点
    for (int i = 0; i<25; i++) {
        ZFJNodeModel *model_f1 = [[ZFJNodeModel alloc] initWithParentNodeModel:nil];
        model_f1.nodeName = [NSString stringWithFormat:@"一级节点%d楼",i];
        model_f1.height = 55;//节点高度
        model_f1.sourceModel = myModel;
        model_f1.nodeCellCls = [MyNodeViewCell class];
        [self.treeView insertNode:model_f1 completed:^(ZFJError * _Nonnull error) {
            NSLog(@"%@",error.message);
        }];
        [self.dataArr_1 addObject:model_f1];
    }
    
    #pragma mark - 添加二级节点
    for (ZFJNodeModel *model_f1 in self.dataArr_1) {
        for (int i = 0; i<2; i++) {
            ZFJNodeModel *model_f2 = [[ZFJNodeModel alloc] initWithParentNodeModel:model_f1];
            model_f2.nodeName = [NSString stringWithFormat:@"二级节点%d楼",i];
            model_f2.height = 55;//节点高度
            //model_f2.sourceModel = myModel;
            model_f2.nodeCellCls = [MyNodeViewCell class];
            [self.treeView insertNode:model_f2 completed:^(ZFJError * _Nonnull error) {
                NSLog(@"%@",error.message);
            }];
            [self.dataArr_2 addObject:model_f2];
        }
    }
    
    #pragma mark - 添加三级节点
    for (ZFJNodeModel *model_f2 in self.dataArr_2) {
        for (int i = 0; i<2; i++) {
            ZFJNodeModel *model_f3 = [[ZFJNodeModel alloc] initWithParentNodeModel:model_f2];
            model_f3.nodeName = [NSString stringWithFormat:@"三级节点%d楼",i];
            model_f3.height = 55;//节点高度
            model_f3.sourceModel = myModel;
            model_f3.nodeCellCls = [MyNodeViewCell class];
            [self.treeView insertNode:model_f3 completed:^(ZFJError * _Nonnull error) {
                NSLog(@"%@",error.message);
            }];
            [self.dataArr_3 addObject:model_f3];
        }
    }
    
    #pragma mark - 添加四级节点
    for (int i = 0; i<3; i++) {
        ZFJNodeModel *model_f3 = [self.dataArr_3 objectAtIndex:i];
        for (int j = 0; j<3; j++) {
            ZFJNodeModel *model_f4 = [[ZFJNodeModel alloc] initWithParentNodeModel:model_f3];
            model_f4.nodeName = [NSString stringWithFormat:@"四级节点%d楼",j];
            model_f4.height = 55;//节点高度
            //model_f4.sourceModel = myModel;
            model_f4.nodeCellCls = [MyNodeViewCell class];
            [self.treeView insertNode:model_f4 completed:^(ZFJError * _Nonnull error) {
                NSLog(@"%@",error.message);
            }];
            [self.dataArr_4 addObject:model_f4];
        }
    }
    
    #pragma mark - 添加五级节点
    for (int i = 0; i<5; i++) {
        ZFJNodeModel *model_f4 = [self.dataArr_4 objectAtIndex:i];
        for (int j = 0; j<3; j++) {
            ZFJNodeModel *model_f5 = [[ZFJNodeModel alloc] initWithParentNodeModel:model_f4];
            model_f5.nodeName = [NSString stringWithFormat:@"五级节点%d楼",j];
            model_f5.height = 55;//节点高度
            model_f5.sourceModel = myModel;
            model_f5.nodeCellCls = [MyNodeViewCell class];
            [self.treeView insertNode:model_f5 completed:^(ZFJError * _Nonnull error) {
                NSLog(@"%@",error.message);
            }];
            [self.dataArr_5 addObject:model_f5];
        }
    }
    
    #pragma mark - 添加六级节点
    for (int i = 0; i<5; i++) {
        ZFJNodeModel *model_f5 = [self.dataArr_5 objectAtIndex:i];
        for (int j = 0; j<3; j++) {
            ZFJNodeModel *model_f6 = [[ZFJNodeModel alloc] initWithParentNodeModel:model_f5];
            model_f6.nodeName = [NSString stringWithFormat:@"六级节点%d楼",j];
            model_f6.height = 55;//节点高度
            //model_f6.sourceModel = myModel;
            model_f6.nodeCellCls = [MyNodeViewCell class];
            [self.treeView insertNode:model_f6 completed:^(ZFJError * _Nonnull error) {
                NSLog(@"%@",error.message);
            }];
            [self.dataArr_6 addObject:model_f6];
        }
    }

    #pragma mark - 添加七级节点
    for (int i = 0; i<5; i++) {
        ZFJNodeModel *model_f6 = [self.dataArr_6 objectAtIndex:i];
        for (int j = 0; j<3; j++) {
            ZFJNodeModel *model_f7 = [[ZFJNodeModel alloc] initWithParentNodeModel:model_f6];
            model_f7.nodeName = [NSString stringWithFormat:@"七级节点%d楼",j];
            model_f7.height = 55;//节点高度
            model_f7.sourceModel = myModel;
            model_f7.nodeCellCls = [MyNodeViewCell class];
            [self.treeView insertNode:model_f7 completed:^(ZFJError * _Nonnull error) {
                NSLog(@"%@",error.message);
            }];
            [self.dataArr_7 addObject:model_f7];
        }
    }
    
    #pragma mark - 添加八级节点
    for (int i = 0; i<5; i++) {
        ZFJNodeModel *model_f7 = [self.dataArr_7 objectAtIndex:i];
        for (int j = 0; j<3; j++) {
            ZFJNodeModel *model_f8 = [[ZFJNodeModel alloc] initWithParentNodeModel:model_f7];
            model_f8.nodeName = [NSString stringWithFormat:@"八级节点%d楼",j];
            model_f8.height = 55;//节点高度
            //model_f8.sourceModel = myModel;
            model_f8.nodeCellCls = [MyNodeViewCell class];
            [self.treeView insertNode:model_f8 completed:^(ZFJError * _Nonnull error) {
                NSLog(@"%@",error.message);
            }];
            [self.dataArr_8 addObject:model_f8];
        }
    }
    
    #pragma mark - 添加九级节点
    for (int i = 0; i<5; i++) {
        ZFJNodeModel *model_f8 = [self.dataArr_8 objectAtIndex:i];
        for (int j = 0; j<3; j++) {
            ZFJNodeModel *model_f9 = [[ZFJNodeModel alloc] initWithParentNodeModel:model_f8];
            model_f9.nodeName = [NSString stringWithFormat:@"九级节点%d楼",j];
            model_f9.height = 55;//节点高度
            model_f9.sourceModel = myModel;
            model_f9.nodeCellCls = [MyNodeViewCell class];
            [self.treeView insertNode:model_f9 completed:^(ZFJError * _Nonnull error) {
                NSLog(@"%@",error.message);
            }];
            [self.dataArr_9 addObject:model_f9];
        }
    }
    
    #pragma mark - 添加十级节点
    for (int i = 0; i<5; i++) {
        ZFJNodeModel *model_f9 = [self.dataArr_9 objectAtIndex:i];
        for (int j = 0; j<3; j++) {
            ZFJNodeModel *model_f10 = [[ZFJNodeModel alloc] initWithParentNodeModel:model_f9];
            model_f10.nodeName = [NSString stringWithFormat:@"十级节点%d楼",j];
            model_f10.height = 55;//节点高度
            //model_f10.sourceModel = myModel;
            model_f10.nodeCellCls = [MyNodeViewCell class];
            [self.treeView insertNode:model_f10 completed:^(ZFJError * _Nonnull error) {
                NSLog(@"%@",error.message);
            }];
            [self.dataArr_10 addObject:model_f10];
        }
    }
}

#pragma mark - ZFJTreeViewDelegate
- (void)treeListView:(ZFJTreeView *)listView didSelectNodeModel:(ZFJNodeModel *)model indexPath:(NSIndexPath *)indexPath{
    //删除
//    [self.treeView deleteNode:model completed:^(ZFJError * _Nonnull error) {
//        NSLog(@"aaaaaaaaa");
//    }];
    //展开、折叠
    [self.treeView expandChildNodes:model completed:^(ZFJError * _Nonnull error) {
        NSLog(@"%@",error.message);
    }];
    
//    [self.treeView expandAllNodes:YES];
}

- (ZFJTreeView *)treeView{
    if(_treeView == nil){
        ZFJTreeViewConfig *model = [[ZFJTreeViewConfig alloc] init];
        model.separatorStyle = UITableViewCellSeparatorStyleNone;
        model.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _treeView = [[ZFJTreeView alloc] initWithFrame:self.view.bounds config:model];
        _treeView.delegate = self;
    }
    return _treeView;
}

- (NSMutableArray *)dataArr_1{
    if(_dataArr_1 == nil){
        _dataArr_1 = [[NSMutableArray alloc] init];
    }
    return _dataArr_1;
}

- (NSMutableArray *)dataArr_2{
    if(_dataArr_2 == nil){
        _dataArr_2 = [[NSMutableArray alloc] init];
    }
    return _dataArr_2;
}

- (NSMutableArray *)dataArr_3{
    if(_dataArr_3 == nil){
        _dataArr_3 = [[NSMutableArray alloc] init];
    }
    return _dataArr_3;
}

- (NSMutableArray *)dataArr_4{
    if(_dataArr_4 == nil){
        _dataArr_4 = [[NSMutableArray alloc] init];
    }
    return _dataArr_4;
}

- (NSMutableArray *)dataArr_5{
    if(_dataArr_5 == nil){
        _dataArr_5 = [[NSMutableArray alloc] init];
    }
    return _dataArr_5;
}

- (NSMutableArray *)dataArr_6{
    if(_dataArr_6 == nil){
        _dataArr_6 = [[NSMutableArray alloc] init];
    }
    return _dataArr_6;
}

- (NSMutableArray *)dataArr_7{
    if(_dataArr_7 == nil){
        _dataArr_7 = [[NSMutableArray alloc] init];
    }
    return _dataArr_7;
}

- (NSMutableArray *)dataArr_8{
    if(_dataArr_8 == nil){
        _dataArr_8 = [[NSMutableArray alloc] init];
    }
    return _dataArr_8;
}

- (NSMutableArray *)dataArr_9{
    if(_dataArr_9 == nil){
        _dataArr_9 = [[NSMutableArray alloc] init];
    }
    return _dataArr_9;
}

- (NSMutableArray *)dataArr_10{
    if(_dataArr_10 == nil){
        _dataArr_10 = [[NSMutableArray alloc] init];
    }
    return _dataArr_10;
}

@end
